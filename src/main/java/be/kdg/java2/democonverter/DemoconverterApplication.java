package be.kdg.java2.democonverter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoconverterApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoconverterApplication.class, args);
    }

}
