package be.kdg.java2.democonverter.dto;

public enum UserType {
    STUDENT, TEACHER, ADMIN, PARENT
}
