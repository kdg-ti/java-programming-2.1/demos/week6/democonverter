package be.kdg.java2.democonverter.converters;

import be.kdg.java2.democonverter.dto.UserType;
import org.springframework.core.convert.converter.Converter;

public class StringToUserTypeConverter implements Converter<String, UserType> {
    @Override
    public UserType convert(String source) {
        if (source.toLowerCase().startsWith("stu")) return UserType.STUDENT;
        if (source.toLowerCase().startsWith("tea")) return UserType.TEACHER;
        if (source.toLowerCase().startsWith("par")) return UserType.PARENT;
        if (source.toLowerCase().startsWith("adm")) return UserType.ADMIN;
        return UserType.STUDENT;//default value...
    }
}
