package be.kdg.java2.democonverter;

import be.kdg.java2.democonverter.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @GetMapping("/adduser")
    public String getUserForm(){
        return "form";
    }

    @PostMapping("/adduser")
    public String postUser(UserDTO userDTO){
        logger.debug(userDTO.toString());
        return "form";
    }
}
